# EpiFi Assignment

### Screenshots

| Screenshot 1     | Screenshot 2    |
| ---------------- | --------------- |
| ![ss1](images/Screenshot_1.png)  | ![ss2](images/Screenshot_2.png)   |

### Features
- Followed <b>MVVM</b> and unidirectional architecture
- Slight use of <b>coroutines</b> to make a fake API call
- Added <b>unit test</b> case for the View Model