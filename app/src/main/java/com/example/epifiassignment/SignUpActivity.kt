package com.example.epifiassignment

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import coil.load
import com.example.epifiassignment.databinding.ActivitySignUpBinding
import com.example.epifiassignment.utils.EPIFI_ICON_URL
import com.example.epifiassignment.utils.getFormattedDate
import com.example.epifiassignment.utils.showDatePickerDialog

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding

    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
        attachObservers()

    }

    private fun setupView() {

        binding.iconImageView.load(EPIFI_ICON_URL)

        binding.panCardEditText.addTextChangedListener {
            viewModel.panCardEntered(it.toString())
        }

        binding.dateOfBirthEditText.setOnClickListener {
            showDatePickerDialog(
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    viewModel.dateOfBirthSelected(year, month, dayOfMonth)
                }
            )
        }

        binding.nextButton.setOnClickListener {
            viewModel.nextButtonClicked()
        }

        binding.dontHavePanButton.setOnClickListener {
            finish()
        }

    }

    private fun attachObservers() {

        viewModel.isPanCardValid.observe(this, Observer { isValid ->
            if (isValid == false) {
                binding.panCardEditText.error = getString(R.string.pan_card_invalid)
            }
        })

        viewModel.dateOfBirth.observe(this, Observer { dateOfBirth ->
            if (dateOfBirth != null) {
                binding.dateOfBirthEditText.setText(getFormattedDate(dateOfBirth))
            }
        })

        viewModel.isNextButtonEnabled.observe(this, Observer { isEnabled ->
            binding.nextButton.isEnabled = isEnabled == true
        })

        viewModel.signUpStatus.observe(this, Observer { status ->
            when (status) {
                is SignUpStatus.Success -> {
                    Toast.makeText(this, R.string.signup_successful, Toast.LENGTH_SHORT).show()
                }
                is SignUpStatus.Error -> {
                    Toast.makeText(this, R.string.signup_error, Toast.LENGTH_SHORT).show()
                }
            }
        })

    }

}