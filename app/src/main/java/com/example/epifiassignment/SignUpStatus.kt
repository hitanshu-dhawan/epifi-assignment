package com.example.epifiassignment

sealed class SignUpStatus {
    data class Success(val authToken: String) : SignUpStatus()
    data class Error(val errorMessage: String) : SignUpStatus()
}