package com.example.epifiassignment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.snakydesign.livedataextensions.combineLatest
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class SignUpViewModel : ViewModel() {


    // PAN Card

    private val _isPanCardValid = MutableLiveData<Boolean>()
    val isPanCardValid: LiveData<Boolean> = _isPanCardValid

    fun panCardEntered(panCard: String) {
        val regex = Regex("[A-Z]{5}[0-9]{4}[A-Z]{1}")

        _isPanCardValid.value = panCard.matches(regex)
    }


    // Date of Birth

    private val _dateOfBirth = MutableLiveData<Date?>()
    val dateOfBirth: LiveData<Date?> = _dateOfBirth

    fun dateOfBirthSelected(year: Int, month: Int, dayOfMonth: Int) {
        try {
            _dateOfBirth.value = Calendar.getInstance().apply { set(year, month, dayOfMonth) }.time
        } catch (e: ArrayIndexOutOfBoundsException) {
            // Log Exception to Crashlytics
        }
    }


    // Next Button

    val isNextButtonEnabled = combineLatest(isPanCardValid, dateOfBirth) { isValid, dateOfBirth ->
        isValid && dateOfBirth != null
    }

    private val _signUpStatus = MutableLiveData<SignUpStatus>()
    val signUpStatus: LiveData<SignUpStatus> = _signUpStatus

    fun nextButtonClicked() {
        viewModelScope.launch {
            delay(1000) // API Call
            _signUpStatus.value = SignUpStatus.Success("...")
        }
    }

}