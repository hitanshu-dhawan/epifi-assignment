package com.example.epifiassignment.utils

import android.app.DatePickerDialog
import android.content.Context
import java.util.*

fun Context.showDatePickerDialog(listener: DatePickerDialog.OnDateSetListener) {
    val calendar = Calendar.getInstance()
    DatePickerDialog(
        this,
        listener,
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    ).show()
}