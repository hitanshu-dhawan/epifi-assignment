package com.example.epifiassignment.utils

import java.text.SimpleDateFormat
import java.util.*

fun getFormattedDate(date: Date): String {
    val formatter = SimpleDateFormat("dd MMMM yyyy", Locale.US)
    return formatter.format(date)
}
