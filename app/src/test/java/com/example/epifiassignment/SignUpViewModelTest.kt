package com.example.epifiassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class SignUpViewModelTest {

    private lateinit var viewModel: SignUpViewModel

    @get:Rule
    var instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = SignUpViewModel()
    }

    @Test
    fun `invalid PAN Card number`() {
        viewModel.panCardEntered("AAAAA11111")

        assert(viewModel.isPanCardValid.value == false)
    }

    @Test
    fun `valid PAN Card number`() {
        viewModel.panCardEntered("AAAAA1111A")

        assert(viewModel.isPanCardValid.value == true)
    }

    @Test
    fun `invalid PAN Card number and DOB`() {
        viewModel.panCardEntered("AAAAA11111")
        viewModel.dateOfBirthSelected(1996, 9, 7)

        viewModel.isNextButtonEnabled.observeForever {}

        assert(viewModel.isNextButtonEnabled.value == false)
    }

    @Test
    fun `valid PAN Card number and DOB`() {
        viewModel.panCardEntered("AAAAA1111A")
        viewModel.dateOfBirthSelected(1996, 9, 7)

        viewModel.isNextButtonEnabled.observeForever {}

        assert(viewModel.isNextButtonEnabled.value == true)
    }

}